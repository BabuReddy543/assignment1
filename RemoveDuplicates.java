import java.util.Scanner;

public class RemoveDuplicates {

	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		StringBuilder sb = new StringBuilder(sc.nextLine());
		for (int i = 0; i < sb.length(); i++) {
			String a = sb.substring(i, i + 1);
			while (sb.indexOf(a) != sb.lastIndexOf(a)) {
				sb.deleteCharAt(sb.lastIndexOf(a));
			}
		}
		System.out.println(sb.toString());
	}
}